$(document).ready(function() {
  const Toast = Swal.mixin({
    toast: true,
    position: 'top-end',
    showConfirmButton: false,
    timer: 3000,
    timerProgressBar: true,
    didOpen: (toast) => {
      toast.addEventListener('mouseenter', Swal.stopTimer)
      toast.addEventListener('mouseleave', Swal.resumeTimer)
    }
  })
    
    $('#userForm').validate({
      rules: {
        name: {
            required: true,
            minlength: 2,
            maxlength: 30,
        },
        lastname: {
            required: true,
            minlength: 2,
            maxlength: 30,
        },
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 5
        },
      },
      messages: {
        name: {
            required: "Por favor ingrese su nombre",
            minlength: "Su nombre debe contener como minimo dos(2) caracteres",
            maxlength: "Su nombre debe contener como maximo treinta(30) caracteres",
        },
        lastname: {
            required: "Por favor ingrese su apellido",
            minlength: "Su apellido debe contener como minimo dos(2) caracteres",
            maxlength: "Su apellido debe contener como maximo treinta(30) caracteres",
        },
        email: {
          required: "Por favor ingrese su email",
          email: "Por favor ingrese un email valido"
        },
        password: {
          required: "Por favor ingrese su password",
          minlength: "Su password debe contener como minimo cinco(5) caracteres"
        }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        

        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Users&action=create',
          dataType: 'json',
          async: false,
          success: function(data) {
            console.log(data);
            var result = data[0];
            console.log(typeof(result[0]));
            if (result[0]) {
               Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'El susuario se registro correctamente'
              }).then(function(){
                  window.location = 'index.php?controller=Users&action=index';
              })
              
            } else {
              Toast.fire({
                icon: 'error',
                title: 'Error',
                text: 'El email ya existe, por favor ingrese otro email'
              })
            }

            //window.location = 'index.php?controller=Users&action=index';
            
          }
        })
      }
    })


    $('#login').validate({
      rules: {
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 5
        },
      },
      messages: {
        email: {
          required: "Por favor ingrese su email",
          email: "Por favor ingrese un email valido"
        },
        password: {
          required: "Por favor ingrese su password",
          minlength: "Su password debe contener como minimo cinco(5) caracteres"
        }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.input-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        

        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Login&action=login',
          dataType: 'json',
          async: false,
          success: function(data) {
            console.log(data);
            var result = data;
            console.log(typeof(result));
            if (result.status == "ok") {
               Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'Bienvenid@ '+result.result[0].name+' !!!'
              }).then(function(){
                  window.location = 'index.php?controller=Index&action=index';
              })
              
            } else if(result.status == "passBad") {
              Toast.fire({
                icon: 'error',
                title: 'Error',
                text: 'Password incorrecto'
              })
            } else if(result.status == "emailBad") {
              Toast.fire({
                icon: 'error',
                title: 'Error',
                text: 'Email no existe'
              })
            }

            
          }
        })
      }

    })

    $('#updateUser').validate({
      rules: {
        name: {
            required: true,
            minlength: 2,
            maxlength: 30,
        },
        lastname: {
            required: true,
            minlength: 2,
            maxlength: 30,
        },
        email: {
          required: true,
          email: true
        },
        password: {
          required: true,
          minlength: 5
        },
      },
      messages: {
        name: {
            required: "Por favor ingrese su nombre",
            minlength: "Su nombre debe contener como minimo dos(2) caracteres",
            maxlength: "Su nombre debe contener como maximo treinta(30) caracteres",
        },
        lastname: {
            required: "Por favor ingrese su apellido",
            minlength: "Su apellido debe contener como minimo dos(2) caracteres",
            maxlength: "Su apellido debe contener como maximo treinta(30) caracteres",
        },
        email: {
          required: "Por favor ingrese su email",
          email: "Por favor ingrese un email valido"
        },
        password: {
          required: "Por favor ingrese su password",
          minlength: "Su password debe contener como minimo cinco(5) caracteres"
        }
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        

        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Users&action=update',
          dataType: 'json',
          async: false,
          success: function(data) {
            console.log(data);
            var result = data[0];
            console.log(typeof(result[0]));
            if (result[0]) {
               Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'El usuario se registro correctamente'
              }).then(function(){
                  window.location = 'index.php?controller=Users&action=index';
              })
              
            } else {
              Toast.fire({
                icon: 'error',
                title: 'Error',
                text: 'El email ya existe, por favor ingrese otro email'
              })
            }

            //window.location = 'index.php?controller=Users&action=index';
            
          }
        })
      }
    })

    $(".delete").on('click', function(e){
      e.preventDefault();
      var id = $(this).attr('data-id');
      var controller = $(this).attr('data-controller');
      Swal.fire({
        icon: 'question',
        title: '¿Quiere borrar el registro?',
        showConfirmButton: true,
        showCancelButton: true,
        confirmButtonText: 'Eliminar',
        cancelButtonText: 'Cancelar'
      }).then((result) => {
        if(result.isConfirmed) {
          $.ajax({
            type: 'post',
            dataType: 'json',
            url: "index.php?controller="+controller+"&action=delete",
            data: {id: id},
            success: function(){
              Toast.fire({
                icon: 'success',
                cancelButtonText: 'Cancelar',
                title: 'Se eliminado correctamente el registro ' + id
              }).then(function(){
                window.location = "index.php?controller="+controller+"&action=index";
            })
            }
          })
        }  
      })
    })
    // Scripts de la entidad Customers
    $('.select2').select2();
    $('#customerForm').validate({
      rules: {
        name: {
            required: true,
            minlength: 2,
            maxlength: 100,
        },
        cuit: {
            required: true,
            number: true,
            minlength: 11,
            maxlength: 11,
        },
        category: {
          required: true,
        },
      },
      messages: {
        name: {
            required: "Por favor ingrese el nombre",
            minlength: "Su nombre debe contener como minimo dos(2) caracteres",
            maxlength: "Su nombre debe contener como maximo treinta(100) caracteres",
        },
        cuit: {
            required: "Por favor ingrese el cuit",
            minlength: "Su cuit debe contener once(11) caracteres",
            maxlength: "Su cuit debe contener once(11) caracteres",
        },
        category: {
          required: "Por favor seleccione una categoria",
        },
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        

        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Customers&action=create',
          dataType: 'json',
          async: false,
          success: function(data) {
            console.log(data);
            var result = data[0];
            console.log(typeof(result[0]));
            if (result[0]) {
               Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'El cliente se registro correctamente'
              }).then(function(){
                  window.location = 'index.php?controller=Customers&action=index';
              })
              
            } else {
              Toast.fire({
                icon: 'error',
                title: 'Error',
                text: 'El cuit ya existe'
              })
            }

            
          }
        })
      }
    })

    // Formulario de resolucion
    $('#resolutionForm').validate({
      rules: {
        resolucion: {
            required: true,
            minlength: 2,
            maxlength: 12,
        },
      },
      messages: {
        resolucion: {
            required: "Por favor ingrese la resolución",
            minlength: "Su nombre debe contener como minimo dos(2) caracteres",
            maxlength: "Su nombre debe contener como maximo doce(12) caracteres",
        },
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        
        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Resolucion&action=create',
          dataType: 'json',
          async: false,
          success: function(data) {
            //console.log(data);
            var result = data[0];
            //console.log(typeof(result[0]));
            if (result[0]) {
              Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'Se ha creado la resolución'
              }).then(function(){
                  window.location = 'index.php?controller=Resolucion&action=index';
              })
            }
          },
          error: function(req,status,error){
            console.log(error);
          }
        })
      }
    })

    $('#updateResolucion').validate({
      rules: {
        resolucion: {
          required: true,
          minlength: 2,
          maxlength: 12,
        },
      },
      messages: {
        resolucion: {
          required: "Por favor ingrese la resolución",
          minlength: "Su nombre debe contener como minimo dos(2) caracteres",
          maxlength: "Su nombre debe contener como maximo doce(12) caracteres",
        },
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        

        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Resolucion&action=update',
          dataType: 'json',
          async: false,
          success: function(data) {
            console.log(data);
            var result = data[0];
            console.log(typeof(result[0]));
            if (result[0]) {
              Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'Se modificó el registro correctamente'
              }).then(function(){
                  window.location = 'index.php?controller=Resolucion&action=index';
              })  
            } else {
              Toast.fire({
                icon: 'error',
                title: 'Error',
                text: 'Hubo un problema, intente nuevamente'
              })
            }

            //window.location = 'index.php?controller=Users&action=index';
            
          }
        })
      }
    })

    // Formulario de backlight
    $('#backlightForm').validate({
      rules: {
        medida: {
          required: true,
          minlength: 2,
        },
        cantidad: {
          required: true,
          number: true,
        },
      },
      messages: {
        medida: {
          required: "Por favor ingrese la medida",
          minlength: "La medida debe contener como minimo dos(2) números",
          number: "La cantidad debe ser un número",
        },
        cantidad: {
          required: "Por favor ingrese la cantidad",
          number: "La cantidad debe ser un número",
        },
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        
        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Backlight&action=create',
          dataType: 'json',
          async: false,
          success: function(data) {
            //console.log(data);
            var result = data[0];
            //console.log(typeof(result[0]));
            if (result[0]) {
              Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'Se ha creado el nuevo backlight'
              }).then(function(){
                  window.location = 'index.php?controller=Backlight&action=index';
              })
            }
          },
          error: function(req,status,error){
            console.log(error);
          }
        })
      }
    })

    $('#updateBacklight').validate({
      rules: {
        medida: {
          required: true,
          minlength: 2,
        },
        cantidad: {
          required: true,
          number: true,
        },
      },
      messages: {
        medida: {
          required: "Por favor ingrese la medida",
          minlength: "La medida debe contener como minimo dos(2) números",
          number: "La cantidad debe ser un número",
        },
        cantidad: {
          required: "Por favor ingrese la cantidad",
          number: "La cantidad debe ser un número",
        },
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        //console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        
        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Backlight&action=update',
          dataType: 'json',
          async: false,
          success: function(data) {
            //console.log(data);
            var result = data[0];
            //console.log(typeof(result[0]));
            if (result[0]) {
              Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'Se ha creado el nuevo backlight'
              }).then(function(){
                  window.location = 'index.php?controller=Backlight&action=index';
              })
            }
          },
          error: function(req,status,error){
            console.log(error);
          }
        })
      }
    });

    // Formulario de Fuente
    $('#fuenteForm').validate({
      rules: {
        medida: {
          required: true,
          minlength: 2,
        },
        cantidad: {
          required: true,
          number: true,
        },
      },
      messages: {
        medida: {
          required: "Por favor ingrese la medida",
          minlength: "La medida debe contener como minimo dos(2) números",
          number: "La cantidad debe ser un número",
        },
        cantidad: {
          required: "Por favor ingrese la cantidad",
          number: "La cantidad debe ser un número",
        },
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        // console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        
        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Fuente&action=create',
          dataType: 'json',
          async: false,
          success: function(data) {
            // console.log(data);
            var result = data[0];
            //console.log(typeof(result[0]));
            if (result[0]) {
              Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'Se ha creado la fuente'
              }).then(function(){
                  window.location = 'index.php?controller=Fuente&action=index';
              })
            }
          },
          error: function(req,status,error){
            console.log(error);
          }
        })
      }
    })

    $('#fuenteUpdate').validate({
      rules: {
        medida: {
          required: true,
          minlength: 2,
        },
        cantidad: {
          required: true,
          number: true,
        },
      },
      messages: {
        medida: {
          required: "Por favor ingrese la medida",
          minlength: "La medida debe contener como minimo dos(2) números",
          number: "La cantidad debe ser un número",
        },
        cantidad: {
          required: "Por favor ingrese la cantidad",
          number: "La cantidad debe ser un número",
        },
      },
      errorElement: 'span',
      errorPlacement: function (error, element) {
        error.addClass('invalid-feedback');
        element.closest('.form-group').append(error);
      },
      highlight: function (element, errorClass, validClass) {
        $(element).addClass('is-invalid');
      },
      unhighlight: function (element, errorClass, validClass) {
        $(element).removeClass('is-invalid');
        $(element).addClass('is-valid');
      },
      submitHandler: function (form) {
        var datos = $(form).serializeArray();
        // console.log(datos);
        //alert( "Form successful submitted!" );
        //form.submit();
        
        $.ajax({
          method: $(form).attr('method'),
          data: datos,
          url: 'index.php?controller=Fuente&action=update',
          dataType: 'json',
          async: false,
          success: function(data) {
            // console.log(data);
            var result = data[0];
            //console.log(typeof(result[0]));
            if (result[0]) {
              Toast.fire({
                icon: 'success',
                title: 'Success',
                text: 'Se ha modificado la fuente'
              }).then(function(){
                  window.location = 'index.php?controller=Fuente&action=index';
              })
            }
          },
          error: function(req,status,error){
            console.log(error);
          }
        })
      }
    })
  
    // Formulario de Panel
  $('#panelForm').validate({
    rules: {
      medida: {
        required: true,
        minlength: 2,
      },
      resolucion: {
        required: true,
      },
      cantidad: {
        required: true,
        number: true,
      },
    },
    messages: {
      medida: {
        required: "Por favor ingrese la medida",
        minlength: "La medida debe contener como mínimo dos(2) números",
        number: "La cantidad debe ser un número",
      },
      resolucion: {
        required: "Por favor ingrese la resolución",
      },
      cantidad: {
        required: "Por favor ingrese la cantidad",
        number: "La cantidad debe ser un número",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
      $(element).addClass('is-valid');
    },
    submitHandler: function (form) {
      var datos = $(form).serializeArray();
      // console.log(datos);
      //alert( "Form successful submitted!" );
      //form.submit();
      
      $.ajax({
        method: $(form).attr('method'),
        data: datos,
        url: 'index.php?controller=Panel&action=create',
        dataType: 'json',
        async: false,
        success: function(data) {
          // console.log(data);
          var result = data[0];
          //console.log(typeof(result[0]));
          if (result[0]) {
            Toast.fire({
              icon: 'success',
              title: 'Success',
              text: 'Se ha creado el panel'
            }).then(function(){
              window.location = 'index.php?controller=Panel&action=index';
            })
          }
        },
        error: function(req,status,error){
          console.log(error);
        }
      })
    }
  })

  $('#panelUpdate').validate({
    rules: {
      medida: {
        required: true,
        minlength: 2,
      },
      resolucion: {
        required: true,
      },
      cantidad: {
        required: true,
        number: true,
      },
    },
    messages: {
      medida: {
        required: "Por favor ingrese la medida",
        minlength: "La medida debe contener como minimo dos(2) números",
        number: "La cantidad debe ser un número",
      },
      resolucion: {
        required: "Por favor ingrese la resolución",
      },
      cantidad: {
        required: "Por favor ingrese la cantidad",
        number: "La cantidad debe ser un número",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
      $(element).addClass('is-valid');
    },
    submitHandler: function (form) {
      var datos = $(form).serializeArray();
      // console.log(datos);
      //alert( "Form successful submitted!" );
      //form.submit();
      
      $.ajax({
        method: $(form).attr('method'),
        data: datos,
        url: 'index.php?controller=Panel&action=update',
        dataType: 'json',
        async: false,
        success: function(data) {
          // console.log(data);
          var result = data[0];
          //console.log(typeof(result[0]));
          if (result[0]) {
            Toast.fire({
              icon: 'success',
              title: 'Success',
              text: 'Se ha modificado el panel'
            }).then(function(){
              window.location = 'index.php?controller=Panel&action=index';
            })
          }
        },
        error: function(req,status,error){
          console.log(error);
        }
      })
    }
  })

  // Formulario de Main
  $('#mainForm').validate({
    rules: {
      resolucion: {
        required: true,
      },
      cantidad: {
        required: true,
        number: true,
      },
    },
    messages: {
      resolucion: {
        required: "Por favor ingrese la resolución",
      },
      cantidad: {
        required: "Por favor ingrese la cantidad",
        number: "La cantidad debe ser un número",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
      $(element).addClass('is-valid');
    },
    submitHandler: function (form) {
      var datos = $(form).serializeArray();
      // console.log(datos);
      //alert( "Form successful submitted!" );
      //form.submit();
      
      $.ajax({
        method: $(form).attr('method'),
        data: datos,
        url: 'index.php?controller=Main&action=create',
        dataType: 'json',
        async: false,
        success: function(data) {
          // console.log(data);
          var result = data[0];
          //console.log(typeof(result[0]));
          if (result[0]) {
            Toast.fire({
              icon: 'success',
              title: 'Success',
              text: 'Se ha creado el main'
            }).then(function(){
              window.location = 'index.php?controller=Main&action=index';
            })
          }
        },
        error: function(req,status,error){
          console.log(error);
        }
      })
    }
  })

  $('#mainUpdate').validate({
    rules: {
      resolucion: {
        required: true,
      },
      cantidad: {
        required: true,
        number: true,
      },
    },
    messages: {
      resolucion: {
        required: "Por favor ingrese la resolución",
      },
      cantidad: {
        required: "Por favor ingrese la cantidad",
        number: "La cantidad debe ser un número",
      },
    },
    errorElement: 'span',
    errorPlacement: function (error, element) {
      error.addClass('invalid-feedback');
      element.closest('.form-group').append(error);
    },
    highlight: function (element, errorClass, validClass) {
      $(element).addClass('is-invalid');
    },
    unhighlight: function (element, errorClass, validClass) {
      $(element).removeClass('is-invalid');
      $(element).addClass('is-valid');
    },
    submitHandler: function (form) {
      var datos = $(form).serializeArray();
      // console.log(datos);
      //alert( "Form successful submitted!" );
      //form.submit();
      
      $.ajax({
        method: $(form).attr('method'),
        data: datos,
        url: 'index.php?controller=Main&action=update',
        dataType: 'json',
        async: false,
        success: function(data) {
          // console.log(data);
          var result = data[0];
          //console.log(typeof(result[0]));
          if (result[0]) {
            Toast.fire({
              icon: 'success',
              title: 'Success',
              text: 'Se ha modificado el main'
            }).then(function(){
              window.location = 'index.php?controller=Main&action=index';
            })
          }
        },
        error: function(req,status,error){
          console.log(error);
        }
      })
    }
  })

});
