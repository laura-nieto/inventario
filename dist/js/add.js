$(document).ready(function() {
    const Toast = Swal.mixin({
        toast: true,
        position: 'top-end',
        showConfirmButton: false,
        timer: 3000,
        timerProgressBar: true,
        didOpen: (toast) => {
          toast.addEventListener('mouseenter', Swal.stopTimer)
          toast.addEventListener('mouseleave', Swal.resumeTimer)
        }
    })

    $(".minus").on('click', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var controller = $(this).attr('data-controller');
        var cantidad = $(this).attr('data-cantidad');
        Swal.fire({
          icon: 'question',
          title: '¿Quiere restar un registro?',
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonText: 'Restar',
          cancelButtonText: 'Cancelar',
          cancelButtonColor: '#d33',
        }).then((result) => {
          if(result.isConfirmed) {
            $.ajax({
              type: 'post',
              dataType: 'json',
              url: "index.php?controller="+controller+"&action=minus",
              data: {
                id: id,
                cantidad: cantidad,
              },
              success: function(data){
                Toast.fire({
                  icon: 'success',
                  cancelButtonText: 'Cancelar',
                  title: 'Se restó un registro del ID: ' + id
                }).then(function(){
                  window.location = "index.php?controller="+controller+"&action=index";
                })
              },
              error: function(error){
                console.log(data);
              }
            })
          }  
        })
    })

    $(".plus").on('click', function(e){
        e.preventDefault();
        var id = $(this).attr('data-id');
        var controller = $(this).attr('data-controller');
        var cantidad = $(this).attr('data-cantidad');
        Swal.fire({
          icon: 'question',
          title: '¿Quiere agregar un registro?',
          showConfirmButton: true,
          showCancelButton: true,
          confirmButtonText: 'Agregar',
          cancelButtonText: 'Cancelar',
          cancelButtonColor: '#d33',
        }).then((result) => {
          if(result.isConfirmed) {
            $.ajax({
              type: 'post',
              dataType: 'json',
              url: "index.php?controller="+controller+"&action=add",
              data: {
                id: id,
                cantidad: cantidad,
              },
              success: function(data){
                Toast.fire({
                  icon: 'success',
                  cancelButtonText: 'Cancelar',
                  title: 'Se agregó un registro del ID: ' + id
                }).then(function(){
                  window.location = "index.php?controller="+controller+"&action=index";
                })
              },
              error: function(error){
                console.log(data);
              }
            })
          }  
        })
    })
})