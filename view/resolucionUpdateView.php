<?php include './view/headerTemplate.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Resolución</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?controller=Resolucion&action=index">Resolución</a></li>
              <li class="breadcrumb-item active">Modificar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Modificar Resolución</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
        <form method="post" id="updateResolucion">
            <div class="form-group">
              <label for="resolucion">Nombre</label>
              <input type="text" class="form-control" name="resolucion" value="<?php echo $resolucion->resolucion; ?>" id="resolucion" placeholder="Ingrese la resolución">
            </div>

        
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <input type="hidden" name="id" value="<?php echo $resolucion->id; ?>">
          <button type="submit" value="modificar" class="btn btn-primary">Modificar  <i class="fa-regular fa-paper-plane"></i></button>
        </div>
        <!-- /.card-footer-->
        </form>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include './view/footerTemplate.php';?>