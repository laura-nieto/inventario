<?php include './view/headerTemplate.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Backlight</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?controller=Backlight&action=index">Backlight</a></li>
              <li class="breadcrumb-item active">Lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Lista de Backlight</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Medida</th>
                    <th>Cantidad</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($backlights as $backlight) { //recorremos el array de objetos y obtenemos el valor de las propiedades '> ?>
                      <tr>
                       <td><?php echo $backlight->id; ?></td>
                       <td><?php echo $backlight->medida; ?></td>
                       <td class="d-flex align-items-center">  
                        <a id="minus" href="#" data-id="<?php echo $backlight->id; ?>" data-cantidad="<?php echo $backlight->cantidad; ?>"data-controller="Backlight" class="btn btn-danger btn-sm minus">
                          <i class="fa-solid fa-minus"></i>
                        </a>

                        <p class="mb-0 mx-3 h6"><?php echo $backlight->cantidad; ?></p>
                        
                        <a id="plus" href="#" data-id="<?php echo $backlight->id; ?>" data-cantidad="<?php echo $backlight->cantidad; ?>" data-controller="Backlight" class="btn btn-success btn-sm plus">
                          <i class="fa-solid fa-plus"></i>
                        </a>
                       </td>
                       <td>
                        <!-- <a id="deleteUser" href="<?php echo $helper->url("Users","delete"); ?>&id=<?php echo $user->id; ?>" class="btn btn-danger"><i class="fa-regular fa-trash-can"></i></a> -->
                        <a id="delete" href="#" data-id="<?php echo $backlight->id; ?>" data-controller="Backlight" class="btn btn-danger delete"><i class="fa-regular fa-trash-can"></i></a>
                        <a href="index.php?controller=Backlight&action=BacklightUpdate&id=<?php echo $backlight->id; ?>" 
                            data-id="<?php echo $backlight->id; ?>" data-medida="<?php echo $backlight->medida; ?>" data-controller="Backlight"class="btn btn-warning update">
                            <i class="fa-regular fa-pen-to-square"></i>
                        </a>
                       </td>
                      </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include './view/footerTemplate.php';?>