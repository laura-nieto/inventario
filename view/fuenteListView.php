<?php include './view/headerTemplate.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Fuentes</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?controller=Resolucion&action=index">Fuentes</a></li>
              <li class="breadcrumb-item active">Lista</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Lista de Fuentes</h3>

          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
          </div>
        </div>
        <div class="card-body">
            <table id="example1" class="table table-bordered table-striped">
                <thead>
                  <tr>
                    <th>Id</th>
                    <th>Medida</th>
                    <th>Smart TV</th>
                    <th>Oled</th>
                    <th>Cantidad</th>
                  </tr>
                </thead>
                <tbody>
                    <?php foreach($fuentes as $fuente) { //recorremos el array de objetos y obtenemos el valor de las propiedades '> ?>
                      <tr>
                       <td><?php echo $fuente->id; ?></td>
                       <td><?php echo $fuente->medida; ?></td>
                       <td><?php echo $fuente->smart_tv ? "Sí":"No"; ?></td>
                       <td><?php echo $fuente->oled ? "Sí":"No"; ?></td>
                       <td class="d-flex align-items-center">  
                        <a id="minus" href="#" data-id="<?php echo $fuente->id; ?>" data-cantidad="<?php echo $fuente->cantidad; ?>"data-controller="Fuente" class="btn btn-danger btn-sm minus">
                          <i class="fa-solid fa-minus"></i>
                        </a>

                        <p class="mb-0 mx-3 h6"><?php echo $fuente->cantidad; ?></p>
                        
                        <a id="plus" href="#" data-id="<?php echo $fuente->id; ?>" data-cantidad="<?php echo $fuente->cantidad; ?>" data-controller="Fuente" class="btn btn-success btn-sm plus">
                          <i class="fa-solid fa-plus"></i>
                        </a>
                       </td>
                       <td>
                        <!-- <a id="deleteUser" href="<?php echo $helper->url("Users","delete"); ?>&id=<?php echo $user->id; ?>" class="btn btn-danger"><i class="fa-regular fa-trash-can"></i></a> -->
                        <a id="delete" href="#" data-id="<?php echo $fuente->id; ?>" data-controller="Fuente" class="btn btn-danger delete"><i class="fa-regular fa-trash-can"></i></a>
                        <a href="index.php?controller=Fuente&action=FuenteUpdate&id=<?php echo $fuente->id; ?>" 
                            data-id="<?php echo $fuente->id; ?>" data-medida="<?php echo $fuente->medida; ?>" data-controller="Fuente"class="btn btn-warning update">
                            <i class="fa-regular fa-pen-to-square"></i>
                        </a>
                       </td>
                      </tr>
                    <?php }?>
                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          
        </div>
        <!-- /.card-footer-->
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
<?php include './view/footerTemplate.php';?>