<?php include './view/headerTemplate.php';?>
  <!-- Content Wrapper. Contains page content -->
  <div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <section class="content-header">
      <div class="container-fluid">
        <div class="row mb-2">
          <div class="col-sm-6">
            <h1>Paneles</h1>
          </div>
          <div class="col-sm-6">
            <ol class="breadcrumb float-sm-right">
              <li class="breadcrumb-item"><a href="index.php?controller=Panel&action=index">Panel</a></li>
              <li class="breadcrumb-item active">Modificar</li>
            </ol>
          </div>
        </div>
      </div><!-- /.container-fluid -->
    </section>

    <!-- Main content -->
    <section class="content">

      <!-- Default box -->
      <div class="card">
        <div class="card-header">
          <h3 class="card-title">Modificar Panel</h3>
          <div class="card-tools">
            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Collapse">
              <i class="fas fa-minus"></i>
            </button>
            <!-- <button type="button" class="btn btn-tool" data-card-widget="remove" title="Remove">
              <i class="fas fa-times"></i>
            </button> -->
          </div>
        </div>
        <div class="card-body">
        <form method="post" id="panelUpdate">
            <div class="form-group">
              <label for="medida">Medida</label>
              <input type="text" class="form-control" name="medida" id="medida" value="<?php echo $panel->medida ?>" placeholder="Ingrese la medida" >
            </div>
            <div class="form-group">
              <label for="resolucion">Resolucion</label>
              <select name="resolucion" id="resolucion" class="form-control">
                <option disabled>Seleccione una resolución</option>
                <?php foreach($resoluciones as $resolucion) {  ?>
                  <option value="<?php echo $resolucion->id ?>"  <?php echo $panel->resolucion_id === $resolucion->id ? "selected" : "" ?> > <?php echo $resolucion->resolucion ?> </option>
                <?php }?>
              </select>
            </div>
            <div class="form-group">
                <div class="form-check">
                    <input type="checkbox" class="form-check-input" name="oled" id="oled" value="1" <?php echo $panel->oled == 1 ? "checked =  checked" : ""; ?>>
                    <label for="oled" class="form-check-label">Oled</label>
                </div>
            </div>
            <div class="form-group">
              <label for="cantidad">Cantidad</label>
              <input type="number" class="form-control" name="cantidad" id="cantidad" placeholder="Ingrese la cantidad" value="<?php echo $panel->cantidad ?>">
            </div>      
        </div>
        <!-- /.card-body -->
        <div class="card-footer">
          <input type="hidden" name="id" value="<?php echo $panel->id; ?>">
          <button type="submit" value="enviar" class="btn btn-primary" >Modificar<i class="fa-regular fa-paper-plane"></i></button>
        </div>
        <!-- /.card-footer-->
        </form>
      </div>
      <!-- /.card -->

    </section>
    <!-- /.content -->
  </div>
  <!-- /.content-wrapper -->
  
<?php include './view/footerTemplate.php';?>
