<?php
class Customer extends EntityBase{
    private $id;
    private $name;
    private $cuit;
    private $category;
     
    public function __construct($adapter) {
        $table="customers";
        parent::__construct($table, $adapter);
    }
     
    public function getId() {
        return $this->id;
    }
 
    public function setId($id) {
        $this->id = $id;
    }
     
    public function getName() {
        return $this->name;
    }
 
    public function setName($name) {
        $this->name = $name;
    }
 
    public function getCuit() {
        return $this->cuit;
    }
 
    public function setCuit($cuit) {
        $this->cuit = $cuit;
    }
 
    public function getCategory() {
        return $this->category;
    }
 
    public function setCategory($category) {
        $this->category = $category;
    }
 
    public function save(){

            $query="INSERT INTO customers (name, cuit, category)
                    VALUES('".$this->name."',
                        '".$this->cuit."',
                        '".$this->category."');";
            $save=$this->db()->query($query);
            $response = array($save);
        return array($response);
    }
    // metodo para realizar la actualizaciond e nuestra entidad
    public function update(){

            $query = "UPDATE customers SET 
            name = '".$this->name."',
            cuit = '".$this->cuit."',
            category = '".$this->category."'
            WHERE (id = '".$this->id."');";
            $save=$this->db()->query($query);
            $response = array($save);
        return array($response);
    }
 
}
?>