<?php
class Backlight extends EntityBase{
    private $id;
    private $medida;
    private $cantidad;
     
    public function __construct($adapter) {
        $table="backlight";
        parent::__construct($table, $adapter);
    }
     
    public function getId() {
        return $this->id;
    }
 
    public function setId($id) {
        $this->id = $id;
    }
     
    public function getMedida() {
        return $this->medida;
    }
 
    public function setMedida($medida) {
        $this->medida = $medida;
    }
 
    public function getCantidad() {
        return $this->cantidad;
    }
 
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
 
    public function save(){

        $query="INSERT INTO backlight (medida, cantidad)
                VALUES('".$this->medida."',
                        '".$this->cantidad."');";
        $save=$this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    // metodo para realizar la actualizaciond e nuestra entidad
    public function update(){

            $query = "UPDATE backlight SET 
            medida = '".$this->medida."',
            cantidad = '".$this->cantidad."
            'WHERE (id = '".$this->id."');";
            $save=$this->db()->query($query);
            $response = array($save);
        return array($response);
    }

    //para restar una cantidad
    public function restarCantidad($id,$cantidad)
    {
        $query = "UPDATE backlight SET
            cantidad = '".$cantidad."'
            WHERE (id = '".$id."');";
        $save = $this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    //para agregar una cantidad
    public function agregarCantidad($id,$cantidad)
    {
        $query = "UPDATE backlight SET
            cantidad = '".$cantidad."'
            WHERE (id = '".$id."');";
        $save = $this->db()->query($query);
        $response = array($save);
        return array($response);
    }
}
?>