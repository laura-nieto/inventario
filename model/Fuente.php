<?php
class Fuente extends EntityBase{
    private $id;
    private $medida;
    private $smart_tv;
    private $oled;
    private $cantidad;
     
    public function __construct($adapter) {
        $table="fuente";
        parent::__construct($table, $adapter);
    }
     
    public function getId() {
        return $this->id;
    }
 
    public function setId($id) {
        $this->id = $id;
    }
     
    public function getMedida() {
        return $this->medida;
    }
 
    public function setMedida($medida) {
        $this->medida = $medida;
    }
 
    public function getSmartTv() {
        return $this->smart_tv;
    }
 
    public function setSmartTv($smart_tv) {
        $this->smart_tv = $smart_tv;
    }

    public function getOled() {
        return $this->oled;
    }
 
    public function setOled($oled) {
        $this->oled = $oled;
    }

    public function getCantidad() {
        return $this->cantidad;
    }
 
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
 
    public function save(){

        $query="INSERT INTO fuente (medida, smart_tv, oled,cantidad)
                VALUES('".$this->medida."',
                        '".$this->smart_tv."',
                        '".$this->oled."',
                        '".$this->cantidad."');";
        $save=$this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    // metodo para realizar la actualizaciond e nuestra entidad
    public function update(){

            $query = "UPDATE fuente SET 
            medida = '".$this->medida."',
            smart_tv = '".$this->smart_tv."',
            oled = '".$this->oled."',
            cantidad = '".$this->cantidad."
            'WHERE (id = '".$this->id."');";
            $save=$this->db()->query($query);
            $response = array($save);
        return array($response);
    }

    //para restar una cantidad
    public function restarCantidad($id,$cantidad)
    {
        $query = "UPDATE fuente SET
            cantidad = '".$cantidad."'
            WHERE (id = '".$id."');";
        $save = $this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    //para agregar una cantidad
    public function agregarCantidad($id,$cantidad)
    {
        $query = "UPDATE fuente SET
            cantidad = '".$cantidad."'
            WHERE (id = '".$id."');";
        $save = $this->db()->query($query);
        $response = array($save);
        return array($response);
    }
}
?>