<?php
class Panel extends EntityBase{
    private $id;
    private $medida;
    private $oled;
    private $resolucion_id;
    private $cantidad;
     
    public function __construct($adapter) {
        $table="panel";
        parent::__construct($table, $adapter);
    }
     
    public function getId() {
        return $this->id;
    }
 
    public function setId($id) {
        $this->id = $id;
    }
     
    public function getMedida() {
        return $this->medida;
    }
 
    public function setMedida($medida) {
        $this->medida = $medida;
    }
 
    public function getOled() {
        return $this->oled;
    }
 
    public function setOled($oled) {
        $this->oled = $oled;
    }

    public function getResolucionId() {
        return $this->resolucion_id;
    }
 
    public function setResolucionId($resolucion_id) {
        $this->resolucion_id = $resolucion_id;
    }

    public function getCantidad() {
        return $this->cantidad;
    }
 
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
 
    public function getAllWithResolucion(){
        $query= "SELECT panel.id, medida, oled, resolucion, cantidad
                    FROM panel
                    INNER JOIN resolucion ON resolucion.id = panel.resolucion_id";
        $all = $this->db()->query($query);

        if($all->num_rows === 0){
            $resultSet = [];
        }else{
            while($row = $all->fetch_object()) {
                $resultSet[]=$row;
            }
        }

        return $resultSet;
    }

    public function save(){

        $query="INSERT INTO panel (medida, oled, resolucion_id ,cantidad)
                VALUES('".$this->medida."',
                        '".$this->oled."',
                        '".$this->resolucion_id."',
                        '".$this->cantidad."');";
        $save=$this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    // metodo para realizar la actualizaciond e nuestra entidad
    public function update(){

        $query = "UPDATE panel SET 
            medida = '".$this->medida."',
            oled = '".$this->oled."',
            resolucion_id = '".$this->resolucion_id."',
            cantidad = '".$this->cantidad."
            'WHERE (id = '".$this->id."');";
        $save=$this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    //para restar una cantidad
    public function restarCantidad($id,$cantidad)
    {
        $query = "UPDATE panel SET
            cantidad = '".$cantidad."'
            WHERE (id = '".$id."');";
        $save = $this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    //para agregar una cantidad
    public function agregarCantidad($id,$cantidad)
    {
        $query = "UPDATE panel SET
            cantidad = '".$cantidad."'
            WHERE (id = '".$id."');";
        $save = $this->db()->query($query);
        $response = array($save);
        return array($response);
    }
}
?>