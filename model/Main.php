<?php
class Main extends EntityBase{
    private $id;
    private $smart_tv;
    private $resolucion_id;
    private $cantidad;
     
    public function __construct($adapter) {
        $table="main";
        parent::__construct($table, $adapter);
    }
     
    public function getId() {
        return $this->id;
    }
 
    public function setId($id) {
        $this->id = $id;
    }
     
    public function getSmartTv() {
        return $this->smart_tv;
    }
 
    public function setSmartTV($smart_tv) {
        $this->smart_tv = $smart_tv;
    }
 
    public function getResolucionId() {
        return $this->resolucion_id;
    }
 
    public function setResolucionId($resolucion_id) {
        $this->resolucion_id = $resolucion_id;
    }

    public function getCantidad() {
        return $this->cantidad;
    }
 
    public function setCantidad($cantidad) {
        $this->cantidad = $cantidad;
    }
 
    public function getAllWithResolucion(){
        $query= "SELECT main.id, smart_tv, resolucion, cantidad
                    FROM main
                    INNER JOIN resolucion ON resolucion.id = main.resolucion_id";
        $all = $this->db()->query($query);

        if($all->num_rows === 0){
            $resultSet = [];
        }else{
            while($row = $all->fetch_object()) {
                $resultSet[]=$row;
            }
        }

        return $resultSet;
    }

    public function save(){

        $query="INSERT INTO main (smart_tv, resolucion_id ,cantidad)
                VALUES('".$this->smart_tv."',
                        '".$this->resolucion_id."',
                        '".$this->cantidad."');";
        $save=$this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    // metodo para realizar la actualizaciond e nuestra entidad
    public function update(){

        $query = "UPDATE main SET 
            smart_tv = '".$this->smart_tv."',
            resolucion_id = '".$this->resolucion_id."',
            cantidad = '".$this->cantidad."
            'WHERE (id = '".$this->id."');";
        $save=$this->db()->query($query);
        $response = array($save);
        return array($response);
    }
 
    //para restar una cantidad
    public function restarCantidad($id,$cantidad)
    {
        $query = "UPDATE main SET
            cantidad = '".$cantidad."'
            WHERE (id = '".$id."');";
        $save = $this->db()->query($query);
        $response = array($save);
        return array($response);
    }

    //para agregar una cantidad
    public function agregarCantidad($id,$cantidad)
    {
        $query = "UPDATE main SET
            cantidad = '".$cantidad."'
            WHERE (id = '".$id."');";
        $save = $this->db()->query($query);
        $response = array($save);
        return array($response);
    }
}
?>