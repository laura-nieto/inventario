<?php
class FuenteController extends ControllerBase{
    public $connect;
    public $adapter;
    public function __construct() {
        parent::__construct();
        $this->connect=new Connection();
        $this->adapter=$this->connect->connect();
    }
     
    public function index(){
         
        //Creamos el objeto usuario
        $fuente=new Fuente($this->adapter);
         
        //Conseguimos todos los usuarios
        $fuentes=$fuente->getAll();
        
        //Cargamos la vista index y le pasamos valores
        $this->view("fuenteList",array(
            "fuentes"=>$fuentes
        ));
    }

    public function fuenteCreate(){
        $this->view("createFuente",[]);
    }
     
    public function create(){
        if(isset($_POST["medida"])){
            //Creamos un backlight
            if (isset($_POST["smart_tv"])) {
                $smart_tv = 1;
            }else{
                $smart_tv = 0;
            }
            if (isset($_POST["oled"])) {
                $oled = 1;
            }else{
                $oled = 0;
            }
            $fuente=new Fuente($this->adapter);
            $fuente->setMedida($_POST["medida"]);
            $fuente->setSmartTv($smart_tv);
            $fuente->setOled($oled);
            $fuente->setCantidad($_POST["cantidad"]);
            $save=$fuente->save();
        }
        die(json_encode($save));
    }

    public function fuenteUpdate(){
        $id=(int)$_GET["id"];
        //Creamos el objeto usuario
        $fuente = new Fuente($this->adapter);
         
        //Conseguimos todos los usuarios
        $fuenteById=$fuente->getById($id);
        //Cargamos la vista index y le pasamos valores
        $this->view("fuenteUpdate",array(
            "fuente"=>$fuenteById,
        ));
    }

    public function update(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
            //Creamos un usuario
            if (isset($_POST["smart_tv"])) {
                $smart_tv = 1;
            }else{
                $smart_tv = 0;
            }
            if (isset($_POST["oled"])) {
                $oled = 1;
            }else{
                $oled = 0;
            }
            $fuente=new Fuente($this->adapter);
            $fuente->setId($id);
            $fuente->setMedida($_POST["medida"]);
            $fuente->setSmartTv($smart_tv);
            $fuente->setOled($oled);
            $fuente->setCantidad($_POST["cantidad"]);
            $save=$fuente->update();
        }
        die(json_encode($save));
    }

    public function delete(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
             
            $fuente=new Fuente($this->adapter);
            $response=$fuente->deleteById($id);
        }
        die(json_encode($response));
    }

    public function minus(){
        $response = false;
        if(isset($_POST["id"]) && isset($_POST["cantidad"])){
            $id=(int)$_POST["id"];
            $cantidad=(int)$_POST["cantidad"]-1;
            $fuente=new Fuente($this->adapter);
            $response=$fuente->restarCantidad($id,$cantidad);
        }
        die(json_encode($response));
    }

    public function add(){
        $response = false;
        
        if(isset($_POST["id"]) && isset($_POST["cantidad"])){
            $id=(int)$_POST["id"];
            $cantidad=(int)$_POST["cantidad"]+1;
            $fuente=new Fuente($this->adapter);
            $response=$fuente->agregarCantidad($id,$cantidad);
        }
        die(json_encode($response));
    }
}
?>