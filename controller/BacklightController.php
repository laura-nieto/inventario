<?php
class BacklightController extends ControllerBase{
    public $connect;
    public $adapter;
    public function __construct() {
        parent::__construct();
        $this->connect=new Connection();
        $this->adapter=$this->connect->connect();
    }
     
    public function index(){
         
        //Creamos el objeto usuario
        $backlight=new Backlight($this->adapter);
         
        //Conseguimos todos los usuarios
        $backlights=$backlight->getAll();
        
        //Cargamos la vista index y le pasamos valores
        $this->view("backligthList",array(
            "backlights"=>$backlights
        ));
    }

    public function backlightCreate(){
        $this->view("createBacklight",[]);
    }
     
    public function create(){
        if(isset($_POST["medida"])){
            //Creamos un backlight
            $backligth=new Backlight($this->adapter);
            $backligth->setMedida($_POST["medida"]);
            $backligth->setCantidad($_POST["cantidad"]);
            $save=$backligth->save();
            
        }
        die(json_encode($save));
    }

    public function backlightUpdate(){
        $id=(int)$_GET["id"];
        //Creamos el objeto usuario
        $backligth = new Backlight($this->adapter);
         
        //Conseguimos todos los usuarios
        $backligthById=$backligth->getById($id);
        //Cargamos la vista index y le pasamos valores
        $this->view("backlightUpdate",array(
            "backlight"=>$backligthById,
        ));
    }

    public function update(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
            //Creamos un usuario
            $backligth=new Backlight($this->adapter);
            $backligth->setId($id);
            $backligth->setMedida($_POST["medida"]);
            $backligth->setCantidad($_POST["cantidad"]);
            $save=$backligth->update();
        }
        die(json_encode($save));
    }

    public function delete(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
             
            $backligth=new Backlight($this->adapter);
            $response=$backligth->deleteById($id);
        }
        die(json_encode($response));
    }

    public function minus(){
        $response = false;
        if(isset($_POST["id"]) && isset($_POST["cantidad"])){
            $id=(int)$_POST["id"];
            $cantidad=(int)$_POST["cantidad"]-1;
            $backlight=new Backlight($this->adapter);
            $response=$backlight->restarCantidad($id,$cantidad);
        }
        die(json_encode($response));
    }

    public function add(){
        $response = false;
        
        if(isset($_POST["id"]) && isset($_POST["cantidad"])){
            $id=(int)$_POST["id"];
            $cantidad=(int)$_POST["cantidad"]+1;
            $backlight=new Backlight($this->adapter);
            $response=$backlight->agregarCantidad($id,$cantidad);
        }
        die(json_encode($response));
    }
}
?>