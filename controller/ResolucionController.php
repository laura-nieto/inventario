<?php
class ResolucionController extends ControllerBase{

    public $connect;
    public $adapter;
    public function __construct() {
        parent::__construct();
        $this->connect=new Connection();
        $this->adapter=$this->connect->connect();
    }

    public function index(){
      
        //Creamos el objeto resolucion
        $resolucion=new Resolucion($this->adapter);
         
        //Conseguimos todos los resolucion
        $resolutions=$resolucion->getAll();
        
        //Cargamos la vista index y le pasamos valores
        $this->view("resolutionList",array(
            "resolutions"=>$resolutions
        ));
    }


    public function createResolucion(){
        $this->view("createResolucion",[]);
    }
     
    public function create(){
        if(isset($_POST["resolucion"])){
            //Creamos una resolucion
            $resolucion=new Resolucion($this->adapter);
            $resolucion->setResolucion($_POST["resolucion"]);
            $save=$resolucion->save();
            
        }
        die(json_encode($save));
    }

    public function resolucionUpdate(){
        $id=(int)$_GET["id"];
        //Creamos el objeto resolucion
        $resolucion=new Resolucion($this->adapter);
         
        //Conseguimos id de resolucion
        $resolucion=$resolucion->getById($id);
        //Cargamos la vista index y le pasamos valores
        $this->view("resolucionUpdate",array(
            "resolucion"=>$resolucion,
        ));
    }

    public function update(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
            //Updateamos la resolucion
            $resolucion=new Resolucion($this->adapter);
            $resolucion->setId($id);
            $resolucion->setResolucion($_POST["resolucion"]);
            $save=$resolucion->update();
            
        }
        die(json_encode($save));
    }

    public function delete(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
             
            $resolucion=new Resolucion($this->adapter);
            $response=$resolucion->deleteById($id);
        }
        die(json_encode($response));
    }
}