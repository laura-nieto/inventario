<?php
class MainController extends ControllerBase{
    public $connect;
    public $adapter;
    public function __construct() {
        parent::__construct();
        $this->connect=new Connection();
        $this->adapter=$this->connect->connect();
    }
     
    public function index(){
         
        //Creamos el objeto usuario
        $main=new Main($this->adapter);
         
        //Conseguimos todos los usuarios
        $mains=$main->getAllWithResolucion();
        
        //Cargamos la vista index y le pasamos valores
        $this->view("mainList",array(
            "mains"=>$mains
        ));
    }

    public function mainCreate(){
        $resolucion = new Resolucion($this->adapter);
        $resoluciones = $resolucion->getAll();
        // var_dump($resoluciones);die;
        $this->view("mainCreate",[
            "resoluciones" => $resoluciones,
        ]);
    }
     
    public function create(){
        if(isset($_POST["cantidad"])){
            //Creamos un backlight
            if (isset($_POST["smart_tv"])) {
                $smart_tv = 1;
            }else{
                $smart_tv = 0;
            }
            
            $main=new Main($this->adapter);
            $main->setSmartTv($smart_tv);
            $main->setResolucionId($_POST["resolucion"]);
            $main->setCantidad($_POST["cantidad"]);
            $save=$main->save();
        }
        die(json_encode($save));
    }

    public function mainUpdate(){
        $id=(int)$_GET["id"];
        //Creamos el objeto usuario
        $main = new Main($this->adapter);
        $resolucion = new Resolucion($this->adapter);
        
        //Conseguimos todos los usuarios
        $mainById=$main->getById($id);
        $resoluciones = $resolucion->getAll();
        //Cargamos la vista index y le pasamos valores
        $this->view("mainUpdate",array(
            "main"=>$mainById,
            "resoluciones"=>$resoluciones,
        ));
    }

    public function update(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
            //Creamos un usuario
            if (isset($_POST["smart_tv"])) {
                $smart_tv = 1;
            }else{
                $smart_tv = 0;
            }
            $main=new Main($this->adapter);
            $main->setId($id);
            $main->setSmartTv($smart_tv);
            $main->setResolucionId($_POST["resolucion"]);
            $main->setCantidad($_POST["cantidad"]);

            $save=$main->update();
        }
        die(json_encode($save));
    }

    public function delete(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
             
            $main=new Main($this->adapter);
            $response=$main->deleteById($id);
        }
        die(json_encode($response));
    }

    public function minus(){
        $response = false;
        if(isset($_POST["id"]) && isset($_POST["cantidad"])){
            $id=(int)$_POST["id"];
            $cantidad=(int)$_POST["cantidad"]-1;
            $main=new Main($this->adapter);
            $response=$main->restarCantidad($id,$cantidad);
        }
        die(json_encode($response));
    }

    public function add(){
        $response = false;
        if(isset($_POST["id"]) && isset($_POST["cantidad"])){
            $id=(int)$_POST["id"];
            $cantidad=(int)$_POST["cantidad"]+1;
            $main=new Main($this->adapter);
            $response=$main->agregarCantidad($id,$cantidad);
        }
        die(json_encode($response));
    }
}
?>