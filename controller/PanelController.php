<?php
class PanelController extends ControllerBase{
    public $connect;
    public $adapter;
    public function __construct() {
        parent::__construct();
        $this->connect=new Connection();
        $this->adapter=$this->connect->connect();
    }
     
    public function index(){
         
        //Creamos el objeto usuario
        $panel=new Panel($this->adapter);
         
        //Conseguimos todos los usuarios
        $paneles=$panel->getAllWithResolucion();
        
        //Cargamos la vista index y le pasamos valores
        $this->view("panelList",array(
            "paneles"=>$paneles
        ));
    }

    public function panelCreate(){
        $resolucion = new Resolucion($this->adapter);
        $resoluciones = $resolucion->getAll();
        // var_dump($resoluciones);die;
        $this->view("panelCreate",[
            "resoluciones" => $resoluciones,
        ]);
    }
     
    public function create(){
        if(isset($_POST["medida"])){
            //Creamos un backlight
            if (isset($_POST["oled"])) {
                $oled = 1;
            }else{
                $oled = 0;
            }
            
            $panel=new Panel($this->adapter);
            $panel->setMedida($_POST["medida"]);
            $panel->setOled($oled);
            $panel->setResolucionId($_POST["resolucion"]);
            $panel->setCantidad($_POST["cantidad"]);
            $save=$panel->save();
        }
        die(json_encode($save));
    }

    public function panelUpdate(){
        $id=(int)$_GET["id"];
        //Creamos el objeto usuario
        $panel = new Panel($this->adapter);
        $resolucion = new Resolucion($this->adapter);
        
        //Conseguimos todos los usuarios
        $panelById=$panel->getById($id);
        $resoluciones = $resolucion->getAll();
        //Cargamos la vista index y le pasamos valores
        $this->view("panelUpdate",array(
            "panel"=>$panelById,
            "resoluciones"=>$resoluciones,
        ));
    }

    public function update(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
            //Creamos un usuario
            if (isset($_POST["oled"])) {
                $oled = 1;
            }else{
                $oled = 0;
            }
            $panel=new Panel($this->adapter);
            $panel->setId($id);
            $panel->setMedida($_POST["medida"]);
            $panel->setOled($oled);
            $panel->setResolucionId($_POST["resolucion"]);
            $panel->setCantidad($_POST["cantidad"]);

            $save=$panel->update();
        }
        die(json_encode($save));
    }

    public function delete(){
        if(isset($_POST["id"])){
            $id=(int)$_POST["id"];
             
            $panel=new Panel($this->adapter);
            $response=$panel->deleteById($id);
        }
        die(json_encode($response));
    }

    public function minus(){
        $response = false;
        if(isset($_POST["id"]) && isset($_POST["cantidad"])){
            $id=(int)$_POST["id"];
            $cantidad=(int)$_POST["cantidad"]-1;
            $panel=new Panel($this->adapter);
            $response=$panel->restarCantidad($id,$cantidad);
        }
        die(json_encode($response));
    }

    public function add(){
        $response = false;
        
        if(isset($_POST["id"]) && isset($_POST["cantidad"])){
            $id=(int)$_POST["id"];
            $cantidad=(int)$_POST["cantidad"]+1;
            $panel=new Panel($this->adapter);
            $response=$panel->agregarCantidad($id,$cantidad);
        }
        die(json_encode($response));
    }
}
?>